# Translit
[![Build Status](https://drone.anufrij.de/api/badges/artemanufrij/translit/status.svg)](https://drone.anufrij.de/artemanufrij/translit)

Translit is a method of encoding Cyrillic letters with Latin ones. You can use it here: https://translit.anufrij.de

For elementary OS users I created a native application written in Vala: https://github.com/artemanufrij/translit

![screenshot](screenshots/app.png)

## Features
* Spell Check
* Languages:
    * Russian
    * Ukrainian
    * Belarusian

## Install from GitLab

### Requirements
``` bash
# Debian based
sudo apt install git nodejs

# Arch based
sudo pacman -S git nodejs
```

### Build
``` bash
# clone this repository
git clone https://gitlab.com/artemanufrij/anufrij-translit.git

# switch into project folder
cd anufrij-translit

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

### Create an AppImage for Linux
``` bash
# (optional) create an electron app for linux
npm run linux-appimage
```

### Donate
<a href="https://www.paypal.me/ArtemAnufrij">PayPal</a> | <a href="https://liberapay.com/Artem/donate">LiberaPay</a> | <a href="https://www.patreon.com/ArtemAnufrij">Patreon</a>
