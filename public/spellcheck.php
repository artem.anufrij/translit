<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Content-Type: application/json; charset=utf-8");

$return_value = "[";
if (isset ($_POST["lang"]) && isset ($_POST["text"])) {
    $lang = $_POST["lang"];
    $text = urldecode($_POST["text"]);

    $words = preg_split("/(\s|\?|\!|\.|\,|\:|\;|\§|\%|\€|\(|\)|\/|\\$|\&|\«|\»|\")/", $text);

    foreach ($words as $word) {
        $word = trim ($word);
        if ($word == "" || preg_match("/(^[a-zA-Z]*$|^[0-9]*$)/", $word)) {
            continue;
        }
        //$word = decode ($word);
        $pspell_link = pspell_new($lang, "", "", "utf-8");
        if (!pspell_check($pspell_link, $word)) {
            if ($return_value != "[") {
                $return_value = $return_value."\t,";
            }
            $return_value = $return_value."{\n\t\"word\": \"".$word."\",\n\t\"lang\": \"".$lang."\",\n\t\"suggest\": [";
            $suggests = pspell_suggest($pspell_link, $word);
            for ($i = 0; $i < count($suggests); $i++) {
                $suggest = $suggests [$i];
                //$suggest = urlencode ($suggest);
                if ($i>0) {
                    $return_value = $return_value.",";
                }
                $return_value = $return_value."\n\t\t\"".$suggest."\"";
            }
            $return_value = $return_value."\n\t]}\n";
        }
    }
}
$return_value = $return_value."]";
echo $return_value;

flush ();
?>
