const electron = require("electron");
const { app, BrowserWindow } = electron;
const express = require("express");
const backend = express();

let win = null;

backend.use(express.static(__dirname + '/dist'));
let server = backend.listen(0, () => {
  app.on("ready", createWindow);
  app.on("activate", () => {
    if (win === null) {
      createWindow();
    }
  });
});

app.on("window-all-closed", () => {
  if (process.platform != "darwin") {
    app.quit();
  }
});

function createWindow() {
  const WINDOW_WIDTH = 948;
  const WINDOW_HEIGHT = 482;

  let bounds = electron.screen.getPrimaryDisplay().bounds;
  let x = bounds.x + (bounds.width - WINDOW_WIDTH) / 2;
  let y = bounds.y + (bounds.height - WINDOW_HEIGHT) / 2;

  win = new BrowserWindow({
    width: WINDOW_WIDTH,
    height: WINDOW_HEIGHT,
    x: x,
    y: y,
    icon: __dirname + "/dist/static/app_48.png"
  });

  win.setMenuBarVisibility(false);
  win.loadURL("http://localhost:" + server.address().port);

  win.on("closed", () => {
    win = null;
  });
}
