import Vue from 'vue'
import VueResource from 'vue-resource';
import App from './App.vue'
import Icon from 'vue-awesome/components/Icon'

import 'vue-awesome/icons'

Vue.component('icon', Icon)

Vue.use(VueResource);

new Vue({
  render: h => h(App),
}).$mount('#app')
